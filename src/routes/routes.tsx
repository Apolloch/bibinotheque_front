// Our route config is just an array of logical "routes"
// with `path` and `component` props, ordered the same

import BeerListPage from "../Pages/BeerListPage/BeerListPage";

// way you'd do inside a `<Switch>`.
export default [
    {
      path: "/",
      component: BeerListPage
    }
  ];
  