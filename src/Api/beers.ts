import { UserRole } from "../Models/User";
import { Beer } from "../Models/Beer";

function timeout(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default {
    async all(): Promise<Beer[]> {
        await timeout(5000);
        return [
            {
                id: 1,
                name: 'Punk IPA',
                type: {
                    id: 1,
                    name: 'IPA',
                    createdAt: '',
                    updatedAt: ''
                },
                brand: {
                    id: 1,
                    name: 'Brewdog',
                    createdAt: '',
                    updatedAt: ''
                },
                evaluations: [
                    {
                        id: 1,
                        user: {
                            id: 1,
                            username: 'Apolloch',
                            email: 'nabacquet@gmail.com',
                            role: UserRole.ADMIN,
                            createdAt: '',
                            updatedAt: ''
                        },
                        beer: {
                            id: 1,
                            name: 'Punk IPA',
                            createdAt: '',
                            updatedAt: ''
                        },
                        note: 4.5,
                        comment: 'C\'est drolement bon',
                        createdAt: '',
                        updatedAt: ''
                    }
                ],
                createdAt: "string",
                updatedAt: "string"
            },
            {
                id: 2,
                name: 'Bellerose',
                type: {
                    id: 1,
                    name: 'IPA',
                    createdAt: '',
                    updatedAt: ''
                },
                brand: {
                    id: 2,
                    name: 'Bellerose',
                    createdAt: '',
                    updatedAt: ''
                },
                evaluations: [
                    {
                        id: 1,
                        user: {
                            id: 1,
                            username: 'Apolloch',
                            email: 'nabacquet@gmail.com',
                            role: UserRole.ADMIN,
                            createdAt: '',
                            updatedAt: ''
                        },
                        beer: {
                            id: 2,
                            name: 'Bellerose',
                            createdAt: '',
                            updatedAt: ''
                        },
                        note: 4.5,
                        comment: 'C\'est drolement bon aussi',
                        createdAt: '',
                        updatedAt: ''
                    }
                ],
                createdAt: "string",
                updatedAt: "string"
            }
        ];
    }
}