import React from 'react';
import './assets/styles/app.scss';
import './assets/styles/vars.scss';
import AppHeader from './Components/AppHeader/AppHeader';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import SideMenu from './Components/SideMenu/SideMenu';
import RouteWithSubRoutes from './routes/RouteWithSubRoutes'
import routes from './routes/routes';
function App() {
  return (
    <div className="App">
      <Router>
        <AppHeader></AppHeader>
        <div className="App-content">
          <SideMenu></SideMenu>
          <Switch>
            {routes.map((route, i) => (
              <RouteWithSubRoutes key={i} {...route} />
            ))}
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
