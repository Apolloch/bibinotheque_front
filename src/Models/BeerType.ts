import { Beer } from "./Beer";

export interface BeerType {
    id: number;
    name: string;
    beers?: Beer[];
    createdAt: string;
    updatedAt: string;

}
