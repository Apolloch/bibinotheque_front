import {Beer} from "./Beer";
export interface Brand {
    id: number;
    name: string;
    beers?: Beer[];
    createdAt: string;
    updatedAt: string;
}
