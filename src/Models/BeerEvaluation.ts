import { Beer } from "./Beer";
import { User } from "./User";

export interface BeerEvaluation {
    id: number;
    user: User;
    beer: Beer;
    note: number;
    comment: string;
    createdAt: string;
    updatedAt: string;
}
