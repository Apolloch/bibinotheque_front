
import {BeerType} from "./BeerType";
import {Brand} from "./Brand";
import {BeerEvaluation} from "./BeerEvaluation";

export interface Beer {
    id: number;
    name: string;
    type?: BeerType;
    brand?: Brand;
    evaluations?: BeerEvaluation[];
    createdAt: string;
    updatedAt: string;
}
