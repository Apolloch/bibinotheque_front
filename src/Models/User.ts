import {BeerEvaluation} from "./BeerEvaluation";

export enum UserRole {
    ADMIN = "ADMIN",
    BASIC = "BASIC",
}
export interface User {
    id: number;
    username: string;
    password?: string;
    email: string;
    role: UserRole;
    evaluations?: BeerEvaluation[];
    createdAt: string;
    updatedAt: string;
}
