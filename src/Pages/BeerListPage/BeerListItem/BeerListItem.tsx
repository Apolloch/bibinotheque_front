import React, { Component } from 'react';
import { Beer } from '../../../Models/Beer';
import './BeerListItem.scss';
interface Props {
    beer: Beer
}

export default class BeerListItem extends Component<Props, any>{

    render() {

        let beer = this.props.beer;

        return (
            <div className="BeerListItem">
                {
                    beer.name
                }
            </div>
        );
    }
}