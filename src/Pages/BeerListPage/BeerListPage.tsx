import React, {Component} from 'react';
import { Beer } from '../../Models/Beer';
import beersApi from '../../Api/beers';
import BeerListItem from './BeerListItem/BeerListItem';
interface State{
    beers : Beer[]
}

export default class BeerListPage extends Component<any, State>{

    
    constructor(props : any) {
        super(props);
        this.state = {
            beers : []
        }
    }
    

    componentDidMount(){
        this.loadBeers();
    }

    render(){

        let beerItems = this.state.beers.map((beer)=>{
            return (
                <BeerListItem key={beer.id} beer={beer}></BeerListItem>
            );
        });

        return(
            <div className="BeerListPage">
                {
                     beerItems
                }
            </div>
        );
    }

    async loadBeers(){
        let beers = await beersApi.all();
        this.setState({
            beers
        })
    }

}