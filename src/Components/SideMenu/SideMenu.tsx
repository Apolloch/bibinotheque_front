import React from 'react'
import { Link } from 'react-router-dom'
import './SideMenu.scss'
export default class extends React.Component {
    render(){
        return (
            <nav className="SideMenu">
                <Link to='/'>Accueil</Link>
            </nav>
        )
    }
}