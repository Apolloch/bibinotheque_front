import React from 'react';
import logo from '../../assets/images/logo.png';
import './AppHeader.scss';

export default class AppHeader extends React.Component{
    render() {
        return (
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h1 className="App-title">
                    Bibinotheque
                </h1>
            </header>
        );
    }
}

